<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Fotos $model */

$this->title = 'Create Fotos';
$this->params['breadcrumbs'][] = ['label' => 'Fotos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fotos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formInsert', [
        'model' => $model,
    ]) ?>

</div>