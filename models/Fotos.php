<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "fotos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $noticia
 *
 * @property Noticias $noticia0
 */
class Fotos extends \yii\db\ActiveRecord
{
    public $archivo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'noticia'], 'integer'],
            //[['nombre'], 'string', 'max' => 200],
            [['id'], 'unique'],
            [
                ['archivo'], 'file',
                'skipOnEmpty' => true, // no es obligatorio seleccionas una imagen
                'extensions' => 'png,jpg' // extensiones permitidas
            ],

            [['noticia'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::class, 'targetAttribute' => ['noticia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'noticia' => 'Noticia',
        ];
    }

    /**
     * Gets query for [[Noticia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticia0()
    {
        return $this->hasOne(Noticias::class, ['id' => 'noticia']);
    }

    public function getNoticias()
    {
        return ArrayHelper::map(Noticias::find()->all(), 'id', 'nombre');
    }

    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/' . $this->id . $this->archivo->name);
        return true;
    }

    /**
     * antes de validar cojo los archivos enviados y
     * los coloco en el modelo
     * 
     */
    public function beforeValidate()
    {
        // si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        }

        return true;
    }


    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate()
    {
        // compruebo si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->nombre = $this->id . $this->archivo->name;
        }

        return true;
    }

    /**
     * metodo que se ejecuta despues de guardar el registro 
     * en la bbdd
     * 
     * @param mixed $insert este argumento es true si estas insertando un registro y false si es una actualizacion
     * @param array $atributosAnteriores tengo todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // pregunto si es una actualizacion
        if (!$insert) {
            // pregunto si he seleccionado un archivo en el formulario
            if (isset($this->archivo)) {
                // compruebo si existia foto anteriormente en la noticia
                if (isset($atributosAnteriores["nombre"]) && $atributosAnteriores["nombre"] != "") {
                    // elimino la imagen vieja del servidor
                    unlink('imgs/' . $atributosAnteriores["nombre"]);
                }
            }
        }
    }

    public function afterDelete()
    {
        // elimino la imagen de la noticia
        // cuando borro la noticia
        if (isset($this->nombre) && file_exists(Yii::getAlias('@webroot') . '/imgs/' . $this->nombre)) {
            unlink('imgs/' . $this->nombre);
        }
        return true;
    }
}
