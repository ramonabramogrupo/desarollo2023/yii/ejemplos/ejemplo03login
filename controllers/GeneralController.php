<?php

namespace app\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class GeneralController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                // acciones del controlador que voy a gestionar
                // con el control de cuentas de usuario
                'only' => ['*'],
                'rules' => [
                    [
                        //acciones que pueden realizar los usuarios logueados
                        'actions' => [], // los usuarios pueden realizar todas las acciones
                        'controllers' => ['site', 'fotos', 'noticias'],
                        'allow' => true,
                        'roles' => ['@'], // @ indica  usuario logueado
                    ],
                    [
                        //acciones que pueden realizar los usuarios no logueados
                        'actions' => [],
                        'controllers' => ['site'],
                        'allow' => true,
                        'roles' => ['?'], // ? indica  usuario no logueado
                    ],
                    [
                        //acciones que pueden realizar los usuarios no logueados
                        'actions' => [''], // los usuarios no pueden realizar ninguna accion
                        'controllers' => ['fotos', 'noticias'],
                        'allow' => true,
                        'roles' => ['?'], // ? indica  usuario no logueado
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}
