<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%noticias}}`.
 */
class m230629_212102_create_noticias_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%noticias}}', [
            'id' => $this->integer(),
            'nombre' => $this->string(100),
            'descripcion' => $this->text(),
            'fecha' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ]);

        $this->addPrimaryKey('primary', 'noticias', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%noticias}}');
    }
}
